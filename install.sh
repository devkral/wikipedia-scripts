#! /bin/sh

if [ "$1" == "upgrade" ]; then
    yarn upgrade --modules-folder "src/static/node_modules"
else
    yarn install --modules-folder "src/static/node_modules"
fi
