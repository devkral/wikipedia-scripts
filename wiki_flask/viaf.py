import requests


def get_viaf_libraries(viaf_id):
    response = requests.get(
        "https://viaf.org/viaf/{}/justlinks.json".format(viaf_id),
        timeout=60
    )
    response.raise_for_status()
    return response.json()
