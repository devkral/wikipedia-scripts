#! /usr/bin/env python3

from flask import Flask, request, render_template, jsonify, abort
from flask_caching import Cache

from . import wikipedia


def create_app():
    app = Flask(__name__)
    cache = Cache(
        app, config={
            'CACHE_TYPE': 'filesystem',
            'CACHE_DIR': "../cachefile"
        }
    )

    @cache.memoize(timeout=6000)
    def retrieve_languages(own_lang=None):
        if own_lang:
            return wikipedia.fetch_languages(own_lang)
        return wikipedia.fetch_languages()

    @cache.memoize(timeout=300)
    def retrieve_q_stats(q):
        return wikipedia.fetch_info(q)

    @cache.memoize(timeout=300)
    def retrieve_name_stats(name, langs=None):
        if langs:
            return wikipedia.fetch_infos_by_name(name, langs)
        return wikipedia.fetch_infos_by_name(name)

    @app.route('/')
    def index():
        return render_template("index.html")

    @app.route('/analyse_name', methods=['POST'])
    def request_stats_name():
        names = request.form.getlist("name")
        langs = request.form.getlist("lang")
        if not names:
            abort(400)
        return jsonify(retrieve_name_stats(names, langs or None))

    @app.route('/languages', methods=['POST'])
    def request_languages():
        own_lang = request.form.get("lang")
        return jsonify(retrieve_languages(own_lang))

    @app.route('/add_authorities', methods=['POST'])
    def add_authorities():
        q = request.form.get("q")
        claims = request.form.getlist("claims")
        targets = request.form.getlist("targets")
        return jsonify(wikipedia.add_authorities(
            q,
            zip(claims, targets),
            app_and_cache=(app, cache),
            ip=request.request.remote_addr
        ))

    return app
