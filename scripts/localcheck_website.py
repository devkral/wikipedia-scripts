#! /usr/bin/env python3

import requests
import json

from urllib.parse import parse_qs
import http.server
from http import HTTPStatus
import socketserver

import rdflib

PORT = 5555


website = b"""
<!DOCTYPE html>
<html>
<head>
    <title>localcheck urls</title>
</head>
<body>
    <form method="POST">
        <input type="url" name="url"/>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
"""


class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.send_header(
            "Content-Type", "text/html"
        )
        self.send_header("Content-Length", str(len(website)))
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(website)

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = parse_qs(self.rfile.read(content_length).decode("utf8"))
        try:
            response = requests.get(
                post_data["url"][0], timeout=10
            )
            body = {
                "error": not response.ok,
                "size": len(response.text),
            }
            if response.ok and len(response.text) > 0:
                if "viaf" in post_data["url"][0]:
                    try:
                        g = rdflib.Graph()
                        g.parse(data=response.text)
                        body["wikidata"] = [
                            str(y) for y in filter(
                                lambda x: (
                                    isinstance(x, rdflib.URIRef) and
                                    "wikidata" in x
                                ),
                                g.objects()
                            )
                        ]
                    except Exception as exc:
                        print(exc)

            dumped = json.dumps(body).encode("utf8")
            self.send_response(HTTPStatus.OK)
            self.send_header(
                "Content-Type", "application/json;charset=utf-8"
            )
            self.send_header("Content-Length", str(len(dumped)))
            self.send_header("Access-Control-Allow-Origin", "*")
            self.end_headers()
            self.wfile.write(dumped)

        except Exception as exc:
            print(exc)
            self.send_error(HTTPStatus.BAD_REQUEST)

    def do_OPTIONS(self):
        self.send_response(HTTPStatus.OK)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        self.end_headers()


def main(address):
    httpd = socketserver.ThreadingTCPServer(address, Handler)
    print("serving at ", end="")
    print(*address, sep=":")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        httpd.shutdown()
        httpd.socket.close()


if __name__ == "__main__":
    main(("127.0.0.1", PORT))
