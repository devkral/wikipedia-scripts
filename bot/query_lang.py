#!/usr/bin/env python3

import pprint

import pywikibot
from pywikibot import pagegenerators as pg

query = """
SELECT ?item
WHERE
{
  ?item wdt:P31 ?type;
        (wdt:P2936) wd:Q8641.
} ORDER BY DESC(?type)
"""


wikidata_site = pywikibot.Site("wikidata", "wikidata")
generator = pg.WikidataSPARQLPageGenerator(
    query, site=wikidata_site
)
for i in list(generator):
    pprint.pprint(i.get())
