#!/usr/bin/env python3

import pprint

import pywikibot
from pywikibot import pagegenerators as pg

query = """
SELECT ?item
WHERE
{
  ?item wdt:P31 ?type;
        (wdt:P103|wdt:P1412|wdt:2936|wdt:P407)* wd:Q8641.
} ORDER BY DESC(?type) LIMIT 1
"""


def main():
    wikidata_site = pywikibot.Site("wikidata", "wikidata")
    generator = pg.WikidataSPARQLPageGenerator(
        query, site=wikidata_site
    )
    for i in list(generator):
        pprint.pprint(i.get())


if __name__ == "__main__":
    main()
